﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeMaster : MonoBehaviour {
	public bool startTimer;
	public GameObject InterOpData;
	public GameObject HUD;
	
	public float timer = 0.0f;

	void Awake() {
		InterOpData = GameObject.FindGameObjectWithTag(Config.InterOpTag);
	}

	void Start() {
		if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.INTRO) {
			timer = Config.getReadyTime + 1;			
		}
		else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
			timer = Config.maxRoundTime;
		}
		else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE2) {
			timer = Config.maxRoundTime;
		}
		else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.OVER) {
			timer = Config.getReadyTime + 1.0f;
		}	
		
		HUD.transform.FindChild("Timer").gameObject.GetComponent<Text>().text = string.Empty;
		HUD.transform.FindChild("Timer").gameObject.SetActive(false);	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(startTimer) {
			if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.INTRO) {
				timer -= Time.deltaTime;
				if(timer < 1.0f) {
					InterOpData.GetComponent<InterOpData>().state = Config.GameState.STAGE1;
					Application.LoadLevel("Stage1");
				}
				HUD.transform.FindChild("Timer").gameObject.GetComponent<Text>().text = ((int)timer).ToString();
				
			}
			else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
				timer -= Time.deltaTime;
				if(timer < 11.0f) {
					if(!HUD.transform.FindChild("Timer").gameObject.activeSelf)
						HUD.transform.FindChild("Timer").gameObject.SetActive(true);

					// last 10 seconds
					HUD.transform.FindChild("Timer").gameObject.GetComponent<Text>().text = ((int)timer).ToString();
				}
				if(timer < 1.0f) {
					// get the players current score
					InterOpData.GetComponent<InterOpData>().player1Score = GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().Score;
					InterOpData.GetComponent<InterOpData>().player2Score = GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().Score;
					InterOpData.GetComponent<InterOpData>().state = Config.GameState.STAGE2;
					Application.LoadLevel("Stage2");
				}
			}
			else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE2) {
				timer -= Time.deltaTime;
				if(timer < 11.0f) {
					if(!HUD.transform.FindChild("Timer").gameObject.activeSelf)
						HUD.transform.FindChild("Timer").gameObject.SetActive(true);

					// last 10 seconds
					HUD.transform.FindChild("Timer").gameObject.GetComponent<Text>().text = ((int)timer).ToString();
				}
				if(timer < 1.0f) {
					// get the players current score
					InterOpData.GetComponent<InterOpData>().player1Score = GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().Score;
					InterOpData.GetComponent<InterOpData>().player2Score = GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().Score;
					InterOpData.GetComponent<InterOpData>().state = Config.GameState.OVER;
					Application.LoadLevel("Over");
				}
			}
			else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.OVER) {
				
			}
		}
	}	

	
	public void SetWhoGoesFirst(GameObject source) {
		if(source.tag == "P1Btn") {
			InterOpData.GetComponent<InterOpData>().whoGoesFirst = Config.player1Tag;
		}
		else {
			InterOpData.GetComponent<InterOpData>().whoGoesFirst = Config.player2Tag;
		}

		startTimer = true;
		HUD.transform.FindChild("P1Btn").gameObject.SetActive(false);
		HUD.transform.FindChild("P2Btn").gameObject.SetActive(false);
		HUD.transform.FindChild("Select").gameObject.SetActive(false);
		HUD.transform.FindChild("Timer").gameObject.SetActive(true);
		GameObject.FindGameObjectWithTag("Keyboard").SetActive(false);
	}
}
