﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DotState : MonoBehaviour, IDotRotation {	
	public GameObject Parent { get; private set; }	
	
	// movement control
	Config.MoveDirection currentDirection;
	bool fly;
	float speed = 4.5f;

	// guidence system
	List<Collider2D> ignoreColliderList = new List<Collider2D>();
	float raycastDistance = 0.05f;
	bool needReposition = false;
	Vector3 reposition;

	// randomize the direction the dot travel
	System.Random rand;

	// Use this for initialization
	void Start () {	
		 rand = new System.Random(System.DateTime.Now.Millisecond);		
	}
	
	// Update is called once per frame
	void Update () {
		if(fly) {
			if(needReposition) {
				this.transform.position = reposition;
				needReposition = false;
			}
			else { 
				Vector3 move = this.transform.position;

				if(currentDirection == Config.MoveDirection.LEFT) move.x -= speed * Time.deltaTime;
				if(currentDirection == Config.MoveDirection.RIGHT) move.x += speed * Time.deltaTime;
				if(currentDirection == Config.MoveDirection.UP) move.y += speed * Time.deltaTime;
				if(currentDirection == Config.MoveDirection.DOWN) move.y -= speed * Time.deltaTime;

				this.transform.position = move;


				// boundary checking
				if(Parent.tag == Config.player1Tag) {
					if(this.transform.position.x <= Calculate.PixelToXCoord(-Config.blockDimension)){
						Parent.GetComponent<PInternalState>().MinusPoint(Config.selfKillMinusPoint);
						Parent.GetComponent<PInternalState>().LostDot(this.gameObject);

						GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.WARP, null);
					}
					else if(this.transform.position.x >= Calculate.PixelToXCoord(Config.screenWidth + Config.blockDimension)) {
						Parent.GetComponent<PInternalState>().AddPoint(Config.bonusPoint);
						Parent.GetComponent<PInternalState>().ReceiveDot(this.gameObject);

						GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.WARP, null);
					}
				}
				else if(Parent.tag == Config.player2Tag) {
					if(this.transform.position.x <= Calculate.PixelToXCoord(-Config.blockDimension)){
						Parent.GetComponent<PInternalState>().AddPoint(Config.bonusPoint);
						Parent.GetComponent<PInternalState>().ReceiveDot(this.gameObject);

						GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.WARP, null);
					}
					else if(this.transform.position.x >= Calculate.PixelToXCoord(Config.screenWidth + Config.blockDimension)) {
						Parent.GetComponent<PInternalState>().MinusPoint(Config.selfKillMinusPoint);
						Parent.GetComponent<PInternalState>().LostDot(this.gameObject);

						GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.WARP, null);
					}
				}

				// bug dot fly up or down out of bound
				if(this.transform.position.y >= Calculate.PixelToYCoord(Config.screenHeight + Config.blockDimension) ||
				   this.transform.position.y <= Calculate.PixelToYCoord(-Config.blockDimension)) {

					Parent.GetComponent<PInternalState>().ReceiveDot(this.gameObject);
					GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.WARP, null);
				}
			}
		}
	}

	void FixedUpdate() {
		RaycastHit2D hit = new RaycastHit2D();
		if(currentDirection == Config.MoveDirection.LEFT)		hit = Physics2D.Raycast(this.transform.position, Vector2.left, raycastDistance, 1 << LayerMask.NameToLayer(Config.layerMaskName));
		else if(currentDirection == Config.MoveDirection.RIGHT) {
			hit = Physics2D.Raycast(this.transform.position, Vector2.right, raycastDistance, 1 << LayerMask.NameToLayer(Config.layerMaskName));
		}
		else if(currentDirection == Config.MoveDirection.UP)	hit = Physics2D.Raycast(this.transform.position, Vector2.up, raycastDistance, 1 << LayerMask.NameToLayer(Config.layerMaskName));
		else if(currentDirection == Config.MoveDirection.DOWN)	hit = Physics2D.Raycast(this.transform.position, Vector2.down, raycastDistance, 1 << LayerMask.NameToLayer(Config.layerMaskName));

		if(hit.collider != null) {
			if(ignoreColliderList.Contains(hit.collider)) return;

			FoundOpening(hit.collider);
			needReposition = true;
			reposition = hit.transform.position;	
			ignoreColliderList.Add(hit.collider);		
		}
	}

	public void NewParent(GameObject parent, Vector3 position) {
		Parent = parent;

		if(parent == null) return;

		Stop();

		// reset animation
		this.GetComponent<DotAnimator>().Reset();

		// set position relative to player 1
		if(parent.tag.Equals(Config.player1Tag)) {			
			this.transform.position = new Vector3(position.x + (this.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f - Calculate.PixelConversion((int)this.GetComponent<SpriteRenderer>().sprite.border.x)), 
												  position.y, 
												  position.z);
			this.transform.parent = parent.transform;

			// set traveling direction
			ChangeDirection(Config.MoveDirection.RIGHT);
		}
		// set position relative to player 2
		else if(parent.tag.Equals(Config.player2Tag)) {
			this.transform.position = new Vector3(position.x - (this.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f - Calculate.PixelConversion((int)this.GetComponent<SpriteRenderer>().sprite.border.x)), 
												  position.y, 
												  position.z);
			this.transform.parent = parent.transform;

			// set traveling direction
			ChangeDirection(Config.MoveDirection.LEFT);
		}
	}

	void RemoveParent() {
		this.transform.parent = null;
	}

	public void Go() {
		fly = true;
		RemoveParent();
		this.GetComponent<DotAnimator>().ToAnimate = true;
	}

	public void Stop() {
		fly = false;
		this.GetComponent<DotAnimator>().ToAnimate = false;
	}

	void OnCollisionEnter2D(Collision2D coll) {
	
		if(coll.gameObject.tag == Config.destroyBlockLeft) {
			if(currentDirection == Config.MoveDirection.RIGHT) {			
				Stop();
				PInternalState parentState = Parent.GetComponent<PInternalState>();
				parentState.MinusPoint(Config.minusPoint);
				parentState.LostDot(this.gameObject);

				GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.EXPLOSION, null);
			}
		}
		else if(coll.gameObject.tag == Config.destroyBlockRight) {
			if(currentDirection == Config.MoveDirection.LEFT) {			
				Stop();
				PInternalState parentState = Parent.GetComponent<PInternalState>();
				parentState.MinusPoint(Config.minusPoint);
				parentState.LostDot(this.gameObject);

				GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.EXPLOSION, null);
			}
		}	
		else if(coll.gameObject.tag == Config.foodTag) {
			coll.gameObject.SetActive(false);
			Parent.GetComponent<PInternalState>().AddPoint(Config.foodPoint);

			GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.CHOMP, null);
		}
		else if(coll.gameObject.tag == Config.enemy1Tag || coll.gameObject.tag == Config.enemy2Tag) {
			// if happen to eat the enemy , get bonus point
			coll.gameObject.SetActive(false);

			GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.EXPLOSION, null);
			Parent.GetComponent<PInternalState>().AddPoint(Config.bonusPoint);
		}		
	}
	

	void OnCollisionExit2D(Collision2D coll) {
		if(ignoreColliderList.Contains(coll.collider)) ignoreColliderList.Remove(coll.collider);
	}

	public void ChangeDirection(Config.MoveDirection direction) {
		currentDirection = direction;
		
		// make sure to update DotAnimator direction
		GetComponent<DotAnimator>().ChangeDirection(direction);
	}

	public void FoundOpening(Collider2D coll) {
		int num = -1;

		// time to turn
		if(coll.gameObject.tag == Config.OpenAllFour) {					
			switch(currentDirection) {
				case Config.MoveDirection.DOWN:
					num = rand.Next(12, 500001) % 2;
					if(Parent.tag == Config.player1Tag) { 
						if(num == 0)			ChangeDirection(Config.MoveDirection.RIGHT);
						else					ChangeDirection(Config.MoveDirection.DOWN);
					}
					else {
						if(num == 0)			ChangeDirection(Config.MoveDirection.LEFT);
						else					ChangeDirection(Config.MoveDirection.DOWN);
					}
					break;

				case Config.MoveDirection.LEFT:					
					if(Parent.tag == Config.player1Tag) {
						num = rand.Next(12, 500001) % 2;
						if(num == 0)			ChangeDirection(Config.MoveDirection.DOWN);
						else					ChangeDirection(Config.MoveDirection.UP);
					}
					else { 	
						num = rand.Next(12, 500001) % 3;				
						if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
						else if(num == 1)			ChangeDirection(Config.MoveDirection.LEFT);
						else						ChangeDirection(Config.MoveDirection.DOWN);
					}			
					break;

				case Config.MoveDirection.UP:
					num = rand.Next(12, 500001) % 2;
					if(Parent.tag == Config.player1Tag) {
						if(num == 0)			ChangeDirection(Config.MoveDirection.UP);
						else					ChangeDirection(Config.MoveDirection.RIGHT);
					}
					else {
						if(num == 0)			ChangeDirection(Config.MoveDirection.UP);
						else					ChangeDirection(Config.MoveDirection.LEFT);
					}
					break;

				case Config.MoveDirection.RIGHT:
					if(Parent.tag == Config.player1Tag) {						
						num = rand.Next(12, 500001) % 3;					
						if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
						else if(num == 1)			ChangeDirection(Config.MoveDirection.RIGHT);
						else						ChangeDirection(Config.MoveDirection.DOWN);
					}
					else {
						num = rand.Next(12, 500001) % 2;
						if(num == 0)			ChangeDirection(Config.MoveDirection.DOWN);
						else					ChangeDirection(Config.MoveDirection.UP);
					}
					break;
			}
		}
		else if(coll.gameObject.tag == Config.OpenBtmRght) {
			if(currentDirection == Config.MoveDirection.UP) ChangeDirection(Config.MoveDirection.RIGHT);
			else								ChangeDirection(Config.MoveDirection.DOWN);
		}
		else if(coll.gameObject.tag == Config.OpenBtmRghtTop) {
			num = rand.Next(12, 500001) % 2;
			if(Parent.tag == Config.player1Tag) {				
				if(currentDirection == Config.MoveDirection.DOWN) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.RIGHT);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.UP) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.RIGHT);
				}
				else if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
			}
			else {
				if(currentDirection == Config.MoveDirection.DOWN) {
												ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.UP)
												ChangeDirection(Config.MoveDirection.UP);
			}
		}
		else if(coll.gameObject.tag == Config.OpenLftBtm) {
			if(currentDirection == Config.MoveDirection.RIGHT) ChangeDirection(Config.MoveDirection.DOWN);
			else if(currentDirection == Config.MoveDirection.UP) ChangeDirection(Config.MoveDirection.LEFT);
		}
		else if(coll.gameObject.tag == Config.OpenLftBtmRght) {
			num = rand.Next(12, 500001) % 2;
			if(Parent.tag == Config.player1Tag) {
				if(currentDirection == Config.MoveDirection.RIGHT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.DOWN);
					else						ChangeDirection(Config.MoveDirection.RIGHT);
				}
				else if(currentDirection == Config.MoveDirection.UP) 
												ChangeDirection(Config.MoveDirection.RIGHT);				
				else if(currentDirection == Config.MoveDirection.LEFT) 
												ChangeDirection(Config.MoveDirection.DOWN);
			}
			else {
				if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.DOWN);
					else						ChangeDirection(Config.MoveDirection.LEFT);
				}
				else if(currentDirection == Config.MoveDirection.UP)
												ChangeDirection(Config.MoveDirection.LEFT);
				else if(currentDirection == Config.MoveDirection.RIGHT)
												ChangeDirection(Config.MoveDirection.DOWN);
			}
		}
		else if(coll.gameObject.tag == Config.OpenLftRght) {
			// just a straight passage way
		}
		else if(coll.gameObject.tag == Config.OpenRghTopLft) {
			num = rand.Next(12, 500001) % 2;
			if(Parent.tag == Config.player1Tag) {
				if(currentDirection == Config.MoveDirection.DOWN)
												ChangeDirection(Config.MoveDirection.RIGHT);
				else if(currentDirection == Config.MoveDirection.RIGHT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.RIGHT);
					else						ChangeDirection(Config.MoveDirection.UP);
				}
				else if(currentDirection == Config.MoveDirection.LEFT)
												ChangeDirection(Config.MoveDirection.UP);
			}
			else {
				if(currentDirection == Config.MoveDirection.DOWN)
												ChangeDirection(Config.MoveDirection.LEFT);
				else if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.LEFT);
					else						ChangeDirection(Config.MoveDirection.UP);
				}
				else if(currentDirection == Config.MoveDirection.RIGHT)
												ChangeDirection(Config.MoveDirection.UP);
			}
		}		
		else if(coll.gameObject.tag == Config.OpenTopBtm) {
			// just a straight passage way
		}
		else if(coll.gameObject.tag == Config.OpenTopLft) {
			if(currentDirection == Config.MoveDirection.RIGHT) ChangeDirection(Config.MoveDirection.UP);
			else if(currentDirection == Config.MoveDirection.DOWN) ChangeDirection(Config.MoveDirection.LEFT);
		}
		else if(coll.gameObject.tag == Config.OpenTopLftBtm) {
			num = rand.Next(12, 500001) % 2;

			if(Parent.tag == Config.player1Tag) {
				if(currentDirection == Config.MoveDirection.DOWN)
												ChangeDirection(Config.MoveDirection.DOWN);
				else if(currentDirection == Config.MoveDirection.RIGHT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.UP)
												ChangeDirection(Config.MoveDirection.UP);
			}
			else {
				if(currentDirection == Config.MoveDirection.UP) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.LEFT);
				}
				else if(currentDirection == Config.MoveDirection.DOWN) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.DOWN);
					else						ChangeDirection(Config.MoveDirection.LEFT);
				}
				else if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
			}
		}
		else if(coll.gameObject.tag == Config.OpenTopRght) {
			if(currentDirection == Config.MoveDirection.LEFT) ChangeDirection(Config.MoveDirection.UP);
			else if(currentDirection == Config.MoveDirection.DOWN) ChangeDirection(Config.MoveDirection.RIGHT);
		}
	}

}
