﻿using UnityEngine;
using System.Collections;

public static class Calculate{
    public static float PixelToYCoord(int y){
        return Config.onePixelInUnitTerm * ((float)y - (Config.screenHeight / 2.0f));
    }

    public static float PixelToXCoord(int x){
        return Config.onePixelInUnitTerm * ((float)x - (Config.screenWidth / 2.0f));
    }

	public static float PixelConversion(int px) {
		return Config.onePixelInUnitTerm * px;
	}
	
}
