﻿using UnityEngine;
using System.Collections.Generic;

public class EnemyHive : MonoBehaviour {
	int maxEnemies;
	List<GameObject> p1EnemiesList;
	List<GameObject> p2EnemiesList;
	int p1eIndex = 0;
	int p2eIndex = 0;

	List<GameObject> bulletsList = new List<GameObject>(100);
	int bulletIndex = 0;

	float p1eDeployTimer = 0.0f;
	float p2eDeployTimer = 0.0f;
	float p1eDeployInterval;
	float p2eDeployInterval;

	GameObject InterOpData;

	void Awake() {
		InterOpData = GameObject.FindGameObjectWithTag(Config.InterOpTag);
		if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
			maxEnemies = 5;
		}
		else {
			maxEnemies = 15;
		}
	}

	// Use this for initialization
	void Start () {
		p1EnemiesList = new List<GameObject>(maxEnemies);
		p2EnemiesList = new List<GameObject>(maxEnemies);

		int i;	
		// create bullets
		for(i = 0; i < bulletsList.Capacity; ++i) {
			GameObject b = Resources.Load<GameObject>("Prefabs/" + Config.enemyBulletTag);
			b.SetActive(false);
			bulletsList.Add(Instantiate<GameObject>(b));
		}
		
		// create enemyies
		for(i = 0; i < maxEnemies; ++i) {
			// enemy 1
			GameObject e1 = Resources.Load<GameObject>("Prefabs/Enemy");
			e1.GetComponent<EnemyState>().parent = this.gameObject;
			e1.SetActive(false);
			p1EnemiesList.Add(Instantiate<GameObject>(e1));
			p1EnemiesList[i].tag = Config.enemy1Tag;

			// enemy 2
			GameObject e2 = Resources.Load<GameObject>("Prefabs/Enemy");
			e2.GetComponent<EnemyState>().parent = this.gameObject;
			e2.SetActive(false);
			p2EnemiesList.Add(Instantiate<GameObject>(e2));
			p2EnemiesList[i].tag = Config.enemy2Tag;
		}			

		if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
			p1eDeployInterval = UnityEngine.Random.Range(0.0f, 4.0f);
			p2eDeployInterval = UnityEngine.Random.Range(0.0f, 4.0f);
		}
		else {
			p1eDeployInterval = UnityEngine.Random.Range(0.0f, 1.0f);
			p2eDeployInterval = UnityEngine.Random.Range(0.0f, 1.0f);
		}

		bulletIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.deltaTime;
		p1eDeployTimer += t;
		p2eDeployTimer += t;

		// player 1 deployment
		if(p1eDeployTimer >= p1eDeployInterval) {
			while(true) {
				if(!p1EnemiesList[p1eIndex].activeSelf) {
					// deploy
					p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().color = new Color(UnityEngine.Random.Range(0.3f, 0.9f), UnityEngine.Random.Range(0.1f, 0.9f), UnityEngine.Random.Range(0.1f, 0.9f));
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().health			= /*UnityEngine.Random.Range(5, 13)*/ 1;
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().speed			= UnityEngine.Random.Range(2, 5);
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().bulletSpeed		= UnityEngine.Random.Range(Config.enemyMinBulletSpd, Config.enemyMaxBulletSpd);
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().timeToLive		= UnityEngine.Random.Range(5, 21);
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().deploy			= true;
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().hasMoveIntoView	= false;
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().fireRate			= UnityEngine.Random.Range(0.3f, 0.8f);
					p1EnemiesList[p1eIndex].GetComponent<EnemyState>().moveInterval		= UnityEngine.Random.Range(0.5f, 1.0f);
					switch(UnityEngine.Random.Range(1, 4)) {
						case 1: p1EnemiesList[p1eIndex].GetComponent<EnemyState>().direction = Config.MoveDirection.DOWN;
							break;
						case 2: p1EnemiesList[p1eIndex].GetComponent<EnemyState>().direction = Config.MoveDirection.LEFT_DOWN;
							break;
						case 3: p1EnemiesList[p1eIndex].GetComponent<EnemyState>().direction = Config.MoveDirection.RIGHT_DOWN;
							break;
					}

					// set starting location
					if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
						switch(p1EnemiesList[p1eIndex].GetComponent<EnemyState>().direction) {
							case Config.MoveDirection.DOWN:
								p1EnemiesList[p1eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.mazeLeftBound) - p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.LEFT_DOWN:
								p1EnemiesList[p1eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.size.x, Calculate.PixelToXCoord(Config.mazeLeftBound) - p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.RIGHT_DOWN:
								p1EnemiesList[p1eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.mazeLeftBound) - p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.size.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
						}
					}
					else {
						switch(p1EnemiesList[p1eIndex].GetComponent<EnemyState>().direction) {
							case Config.MoveDirection.DOWN:
								p1EnemiesList[p1eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.screenWidth) - p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.LEFT_DOWN:
								p1EnemiesList[p1eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.size.x, Calculate.PixelToXCoord(Config.screenWidth) - p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.RIGHT_DOWN:
								p1EnemiesList[p1eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.screenWidth) - p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.size.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p1EnemiesList[p1eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
						}
					}
					p1EnemiesList[p1eIndex].SetActive(true);

					++p1eIndex;
					if(p1eIndex >= maxEnemies) {
						p1eIndex = 0;
					}

					// reset timer 
					p1eDeployTimer = 0.0f;

					if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
						p1eDeployInterval = UnityEngine.Random.Range(1.0f, 5.0f);
					}
					else {
						p1eDeployInterval = UnityEngine.Random.Range(0.3f, 2.0f);
					}
					break;
				}
				
				++p1eIndex;
				if(p1eIndex >= maxEnemies) {
					// end of list try again next update for any free enemies
					p1eIndex = 0;
					break;
				}
			}
		}

		// player 2 deployment 
		if(p2eDeployTimer >= p2eDeployInterval) {
			while(true) {
				if(!p2EnemiesList[p2eIndex].activeSelf) {
					// deploy
					p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().color = new Color(UnityEngine.Random.Range(0.3f, 0.9f), UnityEngine.Random.Range(0.1f, 0.9f), UnityEngine.Random.Range(0.1f, 0.9f));
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().health			= /*UnityEngine.Random.Range(5, 13)*/ 1;
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().speed			= UnityEngine.Random.Range(2, 5);
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().bulletSpeed		= UnityEngine.Random.Range(Config.enemyMinBulletSpd, Config.enemyMaxBulletSpd);
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().timeToLive		= UnityEngine.Random.Range(5, 21);
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().deploy			= true;
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().hasMoveIntoView	= false;
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().fireRate			= UnityEngine.Random.Range(0.3f, 0.8f);
					p2EnemiesList[p2eIndex].GetComponent<EnemyState>().moveInterval		= UnityEngine.Random.Range(0.5f, 1.0f);
					switch(UnityEngine.Random.Range(1, 4)) {
						case 1: p2EnemiesList[p2eIndex].GetComponent<EnemyState>().direction = Config.MoveDirection.DOWN;
							break;
						case 2: p2EnemiesList[p2eIndex].GetComponent<EnemyState>().direction = Config.MoveDirection.LEFT_DOWN;
							break;
						case 3: p2EnemiesList[p2eIndex].GetComponent<EnemyState>().direction = Config.MoveDirection.RIGHT_DOWN;
							break;
					}

					// set starting location
					if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
						switch(p2EnemiesList[p2eIndex].GetComponent<EnemyState>().direction) {
							case Config.MoveDirection.DOWN:
								p2EnemiesList[p2eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(Config.mazeRightBound) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.screenWidth) - p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.LEFT_DOWN:
								p2EnemiesList[p2eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(Config.mazeRightBound) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.size.x, Calculate.PixelToXCoord(Config.screenWidth) - p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.RIGHT_DOWN:
								p2EnemiesList[p2eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(Config.mazeRightBound) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.screenWidth) - p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.size.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
						}
					}
					else {
						switch(p2EnemiesList[p2eIndex].GetComponent<EnemyState>().direction) {
							case Config.MoveDirection.DOWN:
								p2EnemiesList[p2eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.screenWidth) - p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.LEFT_DOWN:
								p2EnemiesList[p2eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.size.x, Calculate.PixelToXCoord(Config.screenWidth) - p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
							case Config.MoveDirection.RIGHT_DOWN:
								p2EnemiesList[p2eIndex].transform.position = 
									new Vector3(
										UnityEngine.Random.Range(Calculate.PixelToXCoord(0) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.x, Calculate.PixelToXCoord(Config.screenWidth) - p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.size.x),
										Calculate.PixelToYCoord(Config.screenHeight) + p2EnemiesList[p2eIndex].GetComponent<SpriteRenderer>().bounds.extents.y,
										0.0f);
								break;
						}
					}

					p2EnemiesList[p2eIndex].SetActive(true);

					++p2eIndex;
					if(p2eIndex >= maxEnemies) {
						p2eIndex = 0;
					}

					// reset timer 
					p2eDeployTimer = 0.0f;

					if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
						p2eDeployInterval = UnityEngine.Random.Range(1.0f, 5.0f);
					}
					else {
						p2eDeployInterval = UnityEngine.Random.Range(0.3f, 2.0f);
					}
					break;
				}
				
				++p2eIndex;
				if(p2eIndex >= maxEnemies) {
					// end of list try again next update for any free enemies
					p2eIndex = 0;
					break;
				}
			}
		}
	}


	public void RequestFire(GameObject fromWho) {
		while(true) {
			if(!bulletsList[bulletIndex].activeSelf) {
				bulletsList[bulletIndex].GetComponent<SpriteRenderer>().color = fromWho.GetComponent<SpriteRenderer>().color;
				bulletsList[bulletIndex].transform.position = fromWho.transform.position;
				bulletsList[bulletIndex].GetComponent<EnemyBulletState>().speed = fromWho.GetComponent<EnemyState>().bulletSpeed;
				bulletsList[bulletIndex].SetActive(true);
				if(fromWho.tag == Config.enemy1Tag) {
					GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.BULLET, Config.enemy1Tag);
				}
				else {
					GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.BULLET, Config.enemy2Tag);
				}
				++bulletIndex;

				if(bulletIndex == bulletsList.Capacity) bulletIndex = 0;
				break;
			}
			
			++bulletIndex;
			if(bulletIndex == bulletsList.Capacity) bulletIndex = 0;
		}
	}

}
