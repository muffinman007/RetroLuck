﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScoreBoardControl : MonoBehaviour {
	public GameObject canvas;

	struct data {
		public bool updateText;
		public bool isIncreasing;
		public int	scoreText;                          // what the Text component shows, this will be our counter
		public GameObject obj;
		public bool isParticleStarted;
		public bool stopUpdate;
	}

	data player1Data;
	data player2Data;

	// help animate the "score" increasing/decreasing in the Text component
	float timer = 0.0f;
	float timeInterval = 0.01f;

	// Use this for initialization
	void Start () {
		PInternalState.ScoreChangeEvent += UpdateScoreText;
		
		player1Data.obj = canvas.transform.FindChild(Config.Player1TextName).gameObject;
		player2Data.obj = canvas.transform.FindChild(Config.Player2TextName).gameObject;

		// place the correct score on screen and internally
		GameObject InterOpData = GameObject.FindGameObjectWithTag(Config.InterOpTag);
		player1Data.scoreText = InterOpData.GetComponent<InterOpData>().player1Score;
		player2Data.scoreText = InterOpData.GetComponent<InterOpData>().player2Score;
		player1Data.obj.GetComponent<Text>().text = player1Data.scoreText.ToString();
		player2Data.obj.GetComponent<Text>().text = player2Data.scoreText.ToString();
	}	
	
	void Update() {
		timer += Time.deltaTime;
		if(timer >= timeInterval) {
			#region update  player 1 score text
			if(player1Data.updateText) {
				if(player1Data.isIncreasing) {
					player1Data.scoreText += 1;

					if(player1Data.scoreText >= GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().Score) {
						player1Data.scoreText = GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().Score;

						player1Data.stopUpdate = true;						
					}
				}
				else {
					player1Data.scoreText -= 1;

					if(player1Data.scoreText <= GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().Score) {
						player1Data.scoreText = GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().Score;

						player1Data.stopUpdate = true;
					}

				}
				
				player1Data.obj.GetComponent<Text>().text = player1Data.scoreText.ToString();	
				
				if(player1Data.stopUpdate) {
					player1Data.updateText = false;
					//player1Data.obj.GetComponent<ParticleSystem>().Stop();
					//player1Data.isParticleStarted = false;
					player1Data.stopUpdate = false;
				}

				// particl system may be annoying 
				//else { 
				//	if(player1Data.isParticleStarted == false) {
				//		player1Data.obj.GetComponent<ParticleSystem>().Play();
				//		player1Data.isParticleStarted = true;
				//	}
				//}

			}
			#endregion

			#region update player 2 score text
			if(player2Data.updateText) {
				if(player2Data.isIncreasing) {
					player2Data.scoreText += 1;

					if(player2Data.scoreText >= GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().Score) {
						player2Data.scoreText = GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().Score;

						player2Data.stopUpdate = true;						
					}
				}
				else {
					player2Data.scoreText -= 1;

					if(player2Data.scoreText <= GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().Score) {
						player2Data.scoreText = GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().Score;

						player2Data.stopUpdate = true;
					}

				}
				
				player2Data.obj.GetComponent<Text>().text = player2Data.scoreText.ToString();	
				
				if(player2Data.stopUpdate) {
					player2Data.updateText = false;
					//player2Data.obj.GetComponent<ParticleSystem>().Stop();
					//player2Data.isParticleStarted = false;
					player2Data.stopUpdate = false;
				}

				//else { 
				//	if(player2Data.isParticleStarted == false) {
				//		player2Data.obj.GetComponent<ParticleSystem>().Play();
				//		player2Data.isParticleStarted = true;
				//	}
				//}
			}
			#endregion

			timer = 0;
		}
	}

	public void UpdateScoreText(int score, GameObject source) {
		if(source.tag == Config.player1Tag) {
			if(player1Data.scoreText < score)	player1Data.isIncreasing = true;
			else								player1Data.isIncreasing = false;
			player1Data.updateText = true;
		}
		else if(source.tag == Config.player2Tag) {
			if(player2Data.scoreText < score)	player2Data.isIncreasing = true;
			else								player2Data.isIncreasing = false;
			player2Data.updateText = true;
		}
	}
	
}
