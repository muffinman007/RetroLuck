﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Initialzation : MonoBehaviour {  
	public GameObject InterOpData;
    void Awake(){
        Screen.SetResolution(Config.screenWidth, Config.screenHeight, false);

        // the math is this: 100 px per 1 unit  so  0.01 unit per 1 pixel
        // so:  size = (desired height in pixel * (1 unit / pixelsPerUnit)) / 2;
        Camera.main.orthographicSize = ( (float)Config.screenHeight * Config.onePixelInUnitTerm ) / 2.0f;		

		InterOpData = GameObject.FindGameObjectWithTag(Config.InterOpTag);
    }

	// Use this for initialization
	void Start(){
		if(InterOpData.GetComponent<InterOpData>().state != Config.GameState.OVER) {
			// setting up players position
			// player1
			GameObject player1 = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Player1"));		
		
			// player2
			GameObject player2 = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Player2"));		

			GameObject dot = null;

			if( (InterOpData.GetComponent<InterOpData>().state == Config.GameState.INTRO) || (InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1)) {
				// creating the dot creature
				dot = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/dotCreature"));
				player1.transform.position = new Vector3(Calculate.PixelToXCoord(Config.mazeLeftBound/2), 0.0f, 0.0f);
				player2.transform.position = new Vector3(Calculate.PixelToXCoord(Config.screenWidth - (Config.mazeLeftBound/2)), 0.0f, 0.0f);		
			}
			else {
				player1.transform.position = new Vector3(Calculate.PixelToXCoord(Config.mazeLeftBound/2), 
														 Calculate.PixelToYCoord(Config.screenHeight / 2) - player1.GetComponent<SpriteRenderer>().bounds.extents.y,
														 0.0f);
				player2.transform.position = new Vector3(Calculate.PixelToXCoord(Config.screenWidth - (Config.mazeLeftBound/2)), 
														 Calculate.PixelToYCoord(Config.screenHeight / 2) - player2.GetComponent<SpriteRenderer>().bounds.extents.y,
														 0.0f);	
			}
		
			if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.INTRO) {
				InterOpData.GetComponent<InterOpData>().player1Score = 0;
				InterOpData.GetComponent<InterOpData>().player2Score = 0;

				if(UnityEngine.Random.Range(0, 2) == 0) {
					player1.GetComponent<PInternalState>().ReceiveDot(dot);
				}
				else {
					player2.GetComponent<PInternalState>().ReceiveDot(dot);
				}
			}
			else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
				if(InterOpData.GetComponent<InterOpData>().whoGoesFirst == Config.player1Tag)
					player1.GetComponent<PInternalState>().ReceiveDot(dot);
				else
					player2.GetComponent<PInternalState>().ReceiveDot(dot);

				GameObject.FindGameObjectWithTag("Timer").GetComponent<TimeMaster>().startTimer = true;
			}
			else if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE2) {
				GameObject.FindGameObjectWithTag("Timer").GetComponent<TimeMaster>().startTimer = true;
			}
		}
		else {
			// game over
			// show score
			GameObject hud = GameObject.FindGameObjectWithTag("OverScore");
			hud.transform.FindChild(Config.Player1TextName).gameObject.GetComponent<Text>().text = InterOpData.GetComponent<InterOpData>().player1Score.ToString();
			hud.transform.FindChild(Config.Player2TextName).gameObject.GetComponent<Text>().text = InterOpData.GetComponent<InterOpData>().player2Score.ToString();
		}				
	}	


}
