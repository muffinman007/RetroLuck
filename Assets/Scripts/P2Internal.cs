﻿using UnityEngine;
using System.Collections;

public class P2Internal : PInternalState {


	protected override void Start() {
		if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
			xCoord = Config.mazeRightBound + Config.margin;
			yCoord = Config.screenHeight;
		}
		else {
			xCoord = 0;
			yCoord = 480;
		}
		base.Start();
	}

	// Update is called once per frame
	protected override void Update () {
		Vector3 move = transform.position;

		// player2 movement
		if(Input.GetKey(KeyCode.Keypad4) || (Input.GetAxis("HorizontalP2") == -1)) move.x	-= (Time.deltaTime * speed);	// left
		if(Input.GetKey(KeyCode.Keypad6) || (Input.GetAxis("HorizontalP2") ==  1)) move.x	+= (Time.deltaTime * speed);	// right
		if(Input.GetKey(KeyCode.Keypad8) || (Input.GetAxis("VerticalP2")   ==  1)) move.y	+= (Time.deltaTime * speed);	// up
		if(Input.GetKey(KeyCode.Keypad5) || (Input.GetAxis("VerticalP2")   == -1)) move.y	-= (Time.deltaTime * speed);	// down

		// boundary check
		Vector3 p2Size = this.GetComponent<SpriteRenderer>().bounds.extents;

		if(move.x <= (Calculate.PixelToXCoord(xCoord) + p2Size.x))
			move.x = Calculate.PixelToXCoord(xCoord) + p2Size.x;
		if(move.x >= (Calculate.PixelToXCoord(Config.screenWidth) - p2Size.x))
			move.x = Calculate.PixelToXCoord(Config.screenWidth) - p2Size.x;
		if(move.y <= (Calculate.PixelToYCoord(0) + p2Size.y))
			move.y = Calculate.PixelToYCoord(0) + p2Size.y;
		if(move.y >= (Calculate.PixelToYCoord(yCoord) - p2Size.y))
			move.y = Calculate.PixelToYCoord(yCoord) - p2Size.y;

		// update player movement
		this.transform.position = move;

		// check fire trigger
		if(HasDot) {
			if(Input.GetKeyUp(Config.p2FireBtn) || Input.GetKeyUp(KeyCode.Joystick2Button0)) FireDotCreature();
			timer = 0.0f;
		}
		else {
			base.Update();
		}
	}
}
