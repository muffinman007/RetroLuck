﻿using UnityEngine;
using System.Collections.Generic;

public class PInternalState : MonoBehaviour {
	public float speed = 4;
	public bool HasDot { get; private set; }
	public int Score { get; private set; }

	// score change event
	public delegate void ScoreChangeHandler(int score, GameObject source);

	public static event ScoreChangeHandler ScoreChangeEvent;

	// fire bullet timer
	protected float timer = 0.0f;
	protected float timeInterval = 0.2f;

	protected const int MAX_BULLETS = 10;
	protected List<GameObject> bulletList = new List<GameObject>(MAX_BULLETS);
	protected int bulletIndex = 0;


	// hit flash
	public Sprite normalTexture;
	public Sprite hitTexture;
	bool flashHitTexture;
	float flashTimer = 0.0f;
	float flashInterval = 0.1f;

	protected GameObject InterOpData;

	protected int xCoord;
	protected int yCoord;

	void Awake() {
		InterOpData = GameObject.FindGameObjectWithTag(Config.InterOpTag);
	}

	virtual protected void Start() {
		// create our bullets
		GameObject bullet;
		for(int i = 0; i < MAX_BULLETS; ++i) {
			if(this.gameObject.tag == Config.player1Tag) {
				bullet = Resources.Load<GameObject>("Prefabs/p1Bullet");				
			}
			else {
				// player 2
				bullet = Resources.Load<GameObject>("Prefabs/p2Bullet");
			}

			bullet.SetActive(false);
			bullet.GetComponent<BulletState>().parent = this.gameObject;
			bulletList.Add(Instantiate<GameObject>(bullet));
		}
	}

	void FixedUpdate() {
		if(flashHitTexture) {
			flashTimer += Time.deltaTime;
			if(flashTimer >= flashInterval) {
				GetComponent<SpriteRenderer>().sprite = normalTexture;
				flashTimer = 0.0f;
				flashHitTexture = false;
			}
		}
	}


	virtual protected void Update() {
		// fire bullet but check hasDot to make sure
		if(!HasDot) {
			timer += Time.deltaTime;
			if(timer >= timeInterval) {
					if(this.gameObject.tag == Config.player1Tag && (Input.GetKey(Config.p1FireBtn) || Input.GetKey(KeyCode.Joystick1Button0)) ){
					FireBullet();
					timer = 0.0f;		
				}
				else if(this.gameObject.tag == Config.player2Tag && (Input.GetKey(Config.p2FireBtn) || Input.GetKey(KeyCode.Joystick2Button0)) ){
					// player 2
					FireBullet();
					timer = 0.0f;
				}
			}
		}		
	}

	void FireBullet() {
		bulletList[bulletIndex].transform.position = new Vector3(this.transform.position.x, 
																 this.transform.position.y + this.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y,
																 0.0f);
		bulletList[bulletIndex].SetActive(true);

		// play sound
		GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.BULLET, this.gameObject.tag);

		++bulletIndex;
		if(bulletIndex == MAX_BULLETS)	bulletIndex = 0;
	}

	// methods 
	public void ResetSpeed() {
		speed = 4;
	}

	public void IncreaseSpeed(float v) {
		speed += v;
	}

	public void DecreaseSpeed(float v) {
		speed -= v;
	}

	// scoring
	public void AddPoint(int point) {
		Score += point;

		if(ScoreChangeEvent != null) ScoreChangeEvent(Score, this.gameObject);
	}

	public void MinusPoint(int point) {
		Score -= point;

		if(ScoreChangeEvent != null) ScoreChangeEvent(Score, this.gameObject);
	}

	// shoot the thing
	public void FireDotCreature() {
		this.GetComponentInChildren<DotState>().Go();
		HasDot = false;

		GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.BULLET, this.gameObject.tag);
	}

	public void ReceiveDot(GameObject dot) {
		HasDot = true;

		// set dot position at the middle of the player bar
		if(this.gameObject.tag == Config.player1Tag) {
			dot.GetComponent<DotState>().NewParent(this.gameObject, 
												   new Vector3(this.transform.position.x + this.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f, this.transform.position.y, 0.0f)
												   );
		}
		else if(this.gameObject.tag == Config.player2Tag) {
			dot.GetComponent<DotState>().NewParent(this.gameObject, 
												   new Vector3(this.transform.position.x - this.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f, this.transform.position.y, 0.0f)
												   );
		}		
	}
	
	public void LostDot(GameObject dot) {
		HasDot = false;
		
		if(gameObject.tag == Config.player1Tag) {
			GameObject.FindGameObjectWithTag(Config.player2Tag).GetComponent<PInternalState>().ReceiveDot(dot);
		}
		else if(gameObject.tag == Config.player2Tag) {
			GameObject.FindGameObjectWithTag(Config.player1Tag).GetComponent<PInternalState>().ReceiveDot(dot);
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		// collision with the dot creature
		if(!HasDot) { 
			if(coll.gameObject.tag == Config.dotCreatureTag) {
				Vector2 contactPt = coll.contacts[0].point;
				Vector3 dotPosition = Vector3.zero;

				if(this.tag == Config.player1Tag) {
					dotPosition = new Vector3(this.gameObject.transform.position.x + this.gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f, 
											  contactPt.y, 
											  0.0f);
				}
				else if(this.tag == Config.player2Tag) {
					dotPosition = new Vector3(this.gameObject.transform.position.x - this.gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f, 
											  contactPt.y, 
											  0.0f);
				}
				coll.gameObject.GetComponent<DotState>().NewParent(this.gameObject, dotPosition);

				this.HasDot = true;
			}
		}

		// collision with enemy bullets
		if(coll.gameObject.tag == Config.enemyBulletTag) {
			// deactivate enemy bullet
			coll.gameObject.SetActive(false);

			// score
			this.MinusPoint(Config.hitMinusPoint);
		
			// flash hit
			flashHitTexture = true;
			GetComponent<SpriteRenderer>().sprite = hitTexture;

			// play sound
			GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.HIT, null);
		}

		// collision with enemy itself
		if(coll.gameObject.tag == Config.enemy1Tag || coll.gameObject.tag == Config.enemy2Tag) {
			// deactivate enemy
			coll.gameObject.SetActive(false);

			this.MinusPoint(Config.enemyKillPoint);

			// flash hit
			flashHitTexture = true;
			GetComponent<SpriteRenderer>().sprite = hitTexture;

			GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.EXPLOSION, null);
		}
	}

}
