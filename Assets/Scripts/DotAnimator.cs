﻿using UnityEngine;
using System.Collections;
using System;

public class DotAnimator : MonoBehaviour, IDotRotation{
	public bool ToAnimate { get; set; }
	public Sprite[] sprites;
	int currentFrame;
	int numOfFrame;

	// sprite only has 3 frames of animation. so this enum help control the animation sequences
	const int FORWARD = 159753;
	const int BACKWARD = 48624;
	int indexQueryDirection = FORWARD;

	float timer = 0.0f;
	float interval = 0.05f;
	
	// Use this for initialization
	void Start () {
		currentFrame = 0;
		numOfFrame = sprites.Length;
	}
	
	// Update is called once per frame
	void Update () {
		if(ToAnimate) {
			timer += Time.deltaTime;
			if(timer >= interval) {
				if(indexQueryDirection == FORWARD) {
					if(++currentFrame == (numOfFrame - 1)) indexQueryDirection = BACKWARD;
				}
				else if(indexQueryDirection == BACKWARD) {
					if(--currentFrame == 0) indexQueryDirection = FORWARD;
				}
				timer = 0.0f;
			}

			this.GetComponent<SpriteRenderer>().sprite = sprites[currentFrame];
		}
	}

	public void Reset() {
		timer = 0.0f;
		currentFrame = 0;
		indexQueryDirection = FORWARD;
		this.GetComponent<SpriteRenderer>().sprite = sprites[currentFrame];
	}

	public void ChangeDirection(Config.MoveDirection direction) {
		if(direction == Config.MoveDirection.RIGHT)
			transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

		else if(direction == Config.MoveDirection.LEFT)
			transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
	}
}
