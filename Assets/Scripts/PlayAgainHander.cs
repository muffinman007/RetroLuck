﻿using UnityEngine;
using System.Collections;

public class PlayAgainHander : MonoBehaviour {

	public void Yes() {
		GameObject.FindGameObjectWithTag(Config.InterOpTag).GetComponent<InterOpData>().state = Config.GameState.INTRO;
		Application.LoadLevel("Intro");
	}

	public void No() {
		Application.Quit();
	}
}
