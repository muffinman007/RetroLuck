﻿using UnityEngine;
using System.Collections;

public class EnemyBulletState : MonoBehaviour {
	public float speed;

	// Update is called once per frame
	void Update () {
		Vector3 move = transform.position;
		move.y -= speed * Time.deltaTime;
		transform.position = move;

		// check boundary
		if( (move.y <= Calculate.PixelToYCoord(-Config.blockDimension)) ||
			(move.y >= Calculate.PixelToYCoord(Config.screenHeight + Config.blockDimension)) ||
			(move.x <= Calculate.PixelToXCoord(-Config.blockDimension)) ||
			(move.x >= Calculate.PixelToYCoord(Config.screenWidth + Config.blockDimension)) ) 
		{
			gameObject.SetActive(false);
		}
	}
}
