﻿using UnityEngine;
using System.Collections;

public class BulletState : MonoBehaviour {
	public GameObject parent;
	public float speed = Config.pBulletSpeed;

	// Update is called once per frame
	void Update () {
		// bound checking
		Vector3 position = transform.position;
		position.y += speed * Time.deltaTime;
		transform.position = position;
		if(transform.position.y >= Calculate.PixelToYCoord(Config.screenHeight + Config.blockDimension)) {
			this.gameObject.SetActive(false);
		}
	}
}
