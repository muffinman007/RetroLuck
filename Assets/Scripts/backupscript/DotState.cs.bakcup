﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DotState : MonoBehaviour, IDotRotation {	
	public GameObject Parent { get; private set; }
	float minDistanceDiff= 0.123f;										// minimum distance different for collision checking

	Config.MoveDirection currentDirection;
	bool fly;
	float speed = 5;

	// guidence system
	Collision2D currentCollision = null;
	List<Collision2D> collisionList = new List<Collision2D>();

	// randomize the direction the dot travel
	System.Random rand;

	// Use this for initialization
	void Start () {	
		 rand = new System.Random(System.DateTime.Now.Millisecond);		
	}
	
	// Update is called once per frame
	void Update () {
		if(fly) {
			Vector3 move = this.transform.position;

			if(currentDirection == Config.MoveDirection.LEFT)	move.x -= speed * Time.deltaTime;
			if(currentDirection == Config.MoveDirection.RIGHT)	move.x += speed * Time.deltaTime;
			if(currentDirection == Config.MoveDirection.UP)		move.y += speed * Time.deltaTime;
			if(currentDirection == Config.MoveDirection.DOWN)	move.y -= speed * Time.deltaTime;

			this.transform.position = move;
		}
	}


	public void NewParent(GameObject parent, Vector3 position) {
		Parent = parent;

		if(parent == null) return;

		Stop();

		// reset animation
		this.GetComponent<DotAnimator>().Reset();

		// set position relative to player 1
		if(parent.tag.Equals(Config.player1Tag)) {			
			this.transform.position = new Vector3(position.x + (this.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f - Calculate.PixelConversion((int)this.GetComponent<SpriteRenderer>().sprite.border.x)), 
												  position.y, 
												  position.z);
			this.transform.parent = parent.transform;

			// set traveling direction
			ChangeDirection(Config.MoveDirection.RIGHT);
		}
		// set position relative to player 2
		else if(parent.tag.Equals(Config.player2Tag)) {
			this.transform.position = new Vector3(position.x - (this.GetComponent<SpriteRenderer>().bounds.size.x / 2.0f - Calculate.PixelConversion((int)this.GetComponent<SpriteRenderer>().sprite.border.x)), 
												  position.y, 
												  position.z);
			this.transform.parent = parent.transform;

			// set traveling direction
			ChangeDirection(Config.MoveDirection.LEFT);
		}
	}

	void RemoveParent() {
		this.transform.parent = null;
	}

	public void Go() {
		fly = true;
		RemoveParent();
		this.GetComponent<DotAnimator>().ToAnimate = true;
	}

	public void Stop() {
		fly = false;
		this.GetComponent<DotAnimator>().ToAnimate = false;
	}

	void OnCollisionEnter2D(Collision2D coll) {
	
		if(coll.gameObject.tag == Config.destroyBlockLeft) {
			if(currentDirection == Config.MoveDirection.RIGHT) {			
				Stop();
				PInternalState parentState = Parent.GetComponent<PInternalState>();
				parentState.MinusPoint(100);
				parentState.LostDot(this.gameObject);
			}
		}
		else if(coll.gameObject.tag == Config.destroyBlockRight) {
			if(currentDirection == Config.MoveDirection.LEFT) {			
				Stop();
				PInternalState parentState = Parent.GetComponent<PInternalState>();
				parentState.MinusPoint(100);
				parentState.LostDot(this.gameObject);
			}
		}
		else { 
			#region check for opening guide
			if(coll.gameObject.tag == Config.OpenAllFour) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);

			}
			else if(coll.gameObject.tag == Config.OpenBottom) {
				// keep moving no need to record coll
				currentCollision = null;

				// this block might be useless. maybe need to get rid of all one opening.
				ChangeDirection(Config.MoveDirection.DOWN);
			}
			else if(coll.gameObject.tag == Config.OpenBtmRght) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenBtmRghtTop) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenLeft) {
				// keep moving no need to record coll
				currentCollision = null;

				// this block might be useless. maybe need to get rid of all one opening.
				ChangeDirection(Config.MoveDirection.LEFT);
			}
			else if(coll.gameObject.tag == Config.OpenLftBtm) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenLftBtmRght) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenLftRght) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenRghTopLft) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenRight) {
				// keep moving no need to record coll
				currentCollision = null;

				// this block might be useless. maybe need to get rid of all one opening.
				ChangeDirection(Config.MoveDirection.RIGHT);
			}
			else if(coll.gameObject.tag == Config.OpenTop) {
				// keep moving no need to record coll
				currentCollision = null;

				// this block might be useless. maybe need to get rid of all one opening.
				ChangeDirection(Config.MoveDirection.UP);
			}
			else if(coll.gameObject.tag == Config.OpenTopBtm) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenTopLft) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenTopLftBtm) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			else if(coll.gameObject.tag == Config.OpenTopRght) {
				if(currentCollision == null) currentCollision = coll;
				else collisionList.Add(coll);
			}
			#endregion
		}
	}

	void OnCollisionStay2D(Collision2D coll) {
		// debug
		if(coll.gameObject.tag == Parent.tag) return;

		// where the dot turns at which opening happens here
		if(currentCollision == null) return;

		if(coll.gameObject.tag == currentCollision.gameObject.tag) {
			float distance = -1.0f;
			float a, b;												// value holder for calcuation.
			
			// first check the distance from dot center to coll center to see if it's very close to 0.0f
			if(currentDirection == Config.MoveDirection.LEFT || currentDirection == Config.MoveDirection.RIGHT) {
				// check the x
				a = Mathf.Abs(gameObject.GetComponent<SpriteRenderer>().bounds.center.x);
				b = Mathf.Abs(coll.collider.bounds.center.x);
				distance = Mathf.Abs(a - b);				
			}
			else if(currentDirection == Config.MoveDirection.UP || currentDirection == Config.MoveDirection.DOWN) {
				// check the y
				a = Mathf.Abs(gameObject.GetComponent<SpriteRenderer>().bounds.center.y);
				b = Mathf.Abs(coll.collider.bounds.center.y);
				distance = Mathf.Abs(a - b);
			}

			if(distance <= minDistanceDiff) {				
				FoundOpening(currentCollision.collider);
				currentCollision = null;
				if(collisionList.Count > 0) {
					currentCollision = collisionList[0];
					collisionList.RemoveAt(0);
				}				
			}
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		// dot had bypass the guide so remove it from the list
		if(currentCollision != null && currentCollision == coll) {
			currentCollision = null;
			if(collisionList.Count > 0) {
					currentCollision = collisionList[0];
					collisionList.RemoveAt(0);
			}
		}
		else if(collisionList.Contains(coll))
			collisionList.Remove(coll);
	}

	public void ChangeDirection(Config.MoveDirection direction) {
		currentDirection = direction;
		
		// make sure to update DotAnimator direction
		GetComponent<DotAnimator>().ChangeDirection(direction);
	}

	public void FoundOpening(Collider2D coll) {
		int num = -1;

		// time to turn
		if(coll.gameObject.tag == Config.OpenAllFour) {					
			switch(currentDirection) {
				case Config.MoveDirection.DOWN:
					num = rand.Next(12, 500001) % 2;
					if(Parent.tag == Config.player1Tag) { 
						if(num == 0)			ChangeDirection(Config.MoveDirection.RIGHT);
						else					ChangeDirection(Config.MoveDirection.DOWN);
					}
					else {
						if(num == 0)			ChangeDirection(Config.MoveDirection.LEFT);
						else					ChangeDirection(Config.MoveDirection.DOWN);
					}
					break;

				case Config.MoveDirection.LEFT:							// only player 2 can go left
					num = rand.Next(12, 500001) % 3;					
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else if(num == 1)			ChangeDirection(Config.MoveDirection.LEFT);
					else						ChangeDirection(Config.MoveDirection.DOWN);					
					break;

				case Config.MoveDirection.UP:
					num = rand.Next(12, 500001) % 2;
					if(Parent.tag == Config.player1Tag) {
						if(num == 0)			ChangeDirection(Config.MoveDirection.UP);
						else					ChangeDirection(Config.MoveDirection.RIGHT);
					}
					else {
						if(num == 0)			ChangeDirection(Config.MoveDirection.UP);
						else					ChangeDirection(Config.MoveDirection.LEFT);
					}
					break;

				case Config.MoveDirection.RIGHT:						// only player 1 can go right
					num = rand.Next(12, 500001) % 3;					
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else if(num == 1)			ChangeDirection(Config.MoveDirection.RIGHT);
					else						ChangeDirection(Config.MoveDirection.DOWN);	
					break;
			}
		}
		else if(coll.gameObject.tag == Config.OpenBtmRght) {
			if(Parent.tag == Config.player1Tag)	ChangeDirection(Config.MoveDirection.RIGHT);
			else								ChangeDirection(Config.MoveDirection.DOWN);
		}
		else if(coll.gameObject.tag == Config.OpenBtmRghtTop) {
			num = rand.Next(12, 500001) % 2;
			if(Parent.tag == Config.player1Tag) {				
				if(currentDirection == Config.MoveDirection.DOWN) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.RIGHT);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.UP) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.RIGHT);
				}
			}
			else {
				if(currentDirection == Config.MoveDirection.DOWN) {
												ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.UP)
												ChangeDirection(Config.MoveDirection.UP);
			}
		}
		else if(coll.gameObject.tag == Config.OpenLftBtm) {
			if(Parent.tag == Config.player1Tag)	ChangeDirection(Config.MoveDirection.DOWN);
			else								ChangeDirection(Config.MoveDirection.LEFT);
		}
		else if(coll.gameObject.tag == Config.OpenLftBtmRght) {
			num = rand.Next(12, 500001) % 2;
			if(Parent.tag == Config.player1Tag) {
				if(currentDirection == Config.MoveDirection.RIGHT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.DOWN);
					else						ChangeDirection(Config.MoveDirection.RIGHT);
				}
				else if(currentDirection == Config.MoveDirection.UP) {
												ChangeDirection(Config.MoveDirection.RIGHT);
				}
			}
			else {
				if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.DOWN);
					else						ChangeDirection(Config.MoveDirection.LEFT);
				}
				else if(currentDirection == Config.MoveDirection.UP)
												ChangeDirection(Config.MoveDirection.LEFT);
			}
		}
		else if(coll.gameObject.tag == Config.OpenLftRght) {
			// just a straight passage way
		}
		else if(coll.gameObject.tag == Config.OpenRghTopLft) {
			num = rand.Next(12, 500001) % 2;
			if(Parent.tag == Config.player1Tag) {
				if(currentDirection == Config.MoveDirection.DOWN)
												ChangeDirection(Config.MoveDirection.RIGHT);
				else if(currentDirection == Config.MoveDirection.RIGHT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.RIGHT);
					else						ChangeDirection(Config.MoveDirection.UP);
				}
			}
			else {
				if(currentDirection == Config.MoveDirection.DOWN)
												ChangeDirection(Config.MoveDirection.LEFT);
				else if(currentDirection == Config.MoveDirection.LEFT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.LEFT);
					else						ChangeDirection(Config.MoveDirection.UP);
				}
			}
		}		
		else if(coll.gameObject.tag == Config.OpenTopBtm) {
			// just a straight passage way
		}
		else if(coll.gameObject.tag == Config.OpenTopLft) {
			if(Parent.tag == Config.player1Tag)	ChangeDirection(Config.MoveDirection.RIGHT);
			else								ChangeDirection(Config.MoveDirection.UP);
		}
		else if(coll.gameObject.tag == Config.OpenTopLftBtm) {
			num = rand.Next(12, 500001) % 2;

			if(Parent.tag == Config.player1Tag) {
				if(currentDirection == Config.MoveDirection.DOWN)
												ChangeDirection(Config.MoveDirection.DOWN);
				else if(currentDirection == Config.MoveDirection.RIGHT) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.DOWN);
				}
				else if(currentDirection == Config.MoveDirection.UP)
												ChangeDirection(Config.MoveDirection.UP);
			}
			else {
				if(currentDirection == Config.MoveDirection.UP) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.UP);
					else						ChangeDirection(Config.MoveDirection.LEFT);
				}
				else if(currentDirection == Config.MoveDirection.DOWN) {
					if(num == 0)				ChangeDirection(Config.MoveDirection.DOWN);
					else						ChangeDirection(Config.MoveDirection.LEFT);
				}
			}
		}
		else if(coll.gameObject.tag == Config.OpenTopRght) {
			if(Parent.tag == Config.player1Tag)	ChangeDirection(Config.MoveDirection.RIGHT);
			else								ChangeDirection(Config.MoveDirection.UP);
		}
	}
}
