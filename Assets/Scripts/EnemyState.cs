﻿using UnityEngine;

public class EnemyState : MonoBehaviour {
	public int health;
	public float speed;
	public float bulletSpeed;
	public float timeToLive;
	public bool deploy = true;
	public bool hasMoveIntoView = false;				// do not check boundary until object is inside playing field

	public float fireRate;	
	float fireTimer = 0.0f;

	public GameObject parent;							// maintain communication with mothership to request fire support

	// for boundary checking
	float yOffset;
	float xOffset;

	public float moveTimer = 0.0f;
	public float moveInterval;
	public Config.MoveDirection direction;
	Vector3 position;

	bool needNewDirection = false;

	GameObject InterOpData;

	void Start() {
		yOffset = GetComponent<SpriteRenderer>().bounds.extents.y;
		xOffset = GetComponent<SpriteRenderer>().bounds.extents.x;	
		UnityEngine.Random.seed = System.DateTime.Now.Millisecond;	

		InterOpData = GameObject.FindGameObjectWithTag(Config.InterOpTag);
	}
	
	// Update is called once per frame
	void Update () {
		if(deploy) {
			timeToLive -= Time.deltaTime;
			if(timeToLive <= 0.0f) {
				#region fly off the screen
				// pick a direction. there's 8 possible direction
				RandNewDirection(true);
				deploy = false;
				#endregion
			}
			else {
				#region continue moving. at specified direction
				moveTimer += Time.deltaTime;
				if(moveTimer >= moveInterval) {
					// get a new direction and a new move interval
					RandNewDirection(false);

					// I just picked 0.3f no reason behind it
					if(timeToLive > 0.3f) {
						moveInterval = UnityEngine.Random.Range(0.3f, timeToLive);
					}
					else {
						// enemy is about to recall so just set the interval greater than timeToLive
						moveInterval = 0.4f;
					}
					moveTimer = 0.0f;					
				}
				else {
					// keep moving 
					Move(false);
				}
				#endregion
			}			
		}
		else {
			#region continue moving at the set direction until out of bound
			Move(true);

			// once fly out of bound, deactive 
			if(position.x <= (Calculate.PixelToXCoord(0) - xOffset) || 
			   position.x >= (Calculate.PixelToXCoord(Config.screenWidth) + xOffset) ||
			   position.y <= (Calculate.PixelToYCoord(0) - yOffset) ||
			   position.y >= (Calculate.PixelToYCoord(Config.screenHeight) + yOffset)){

				gameObject.SetActive(false);
			}
			#endregion
		}
		
		// start firing when in bound
		if(hasMoveIntoView) {
			// keep firing until deactived
			fireTimer += Time.deltaTime;
			if(fireTimer >= fireRate) {
				parent.GetComponent<EnemyHive>().RequestFire(this.gameObject);
				fireTimer = 0.0f;
			}
		}
	}


	void Move(bool returnToBase) {
		position = transform.position;
			
		if(direction == Config.MoveDirection.DOWN) {
			position.y -= speed * Time.deltaTime;
		}
		else if(direction == Config.MoveDirection.LEFT) {
			position.x -= speed * Time.deltaTime;
		}
		else if(direction == Config.MoveDirection.UP) {
			position.y += speed * Time.deltaTime;
		}
		else if(direction == Config.MoveDirection.RIGHT) {
			position.x += speed * Time.deltaTime;
		}else if(direction == Config.MoveDirection.LEFT_DOWN) {
			position.x -= speed * Time.deltaTime;
			position.y -= speed * Time.deltaTime;
		}
		else if(direction == Config.MoveDirection.LEFT_UP) {
			position.x -= speed * Time.deltaTime;
			position.y += speed * Time.deltaTime;
		}
		else if(direction == Config.MoveDirection.RIGHT_UP) {
			position.x += speed * Time.deltaTime;
			position.y += speed * Time.deltaTime;
		}
		else if(direction == Config.MoveDirection.RIGHT_DOWN) {
			position.x += speed * Time.deltaTime;
			position.y -= speed * Time.deltaTime;
		}
		//else if(direction == Config.MoveDirection.STAY_STILL) {
		//	// do nothing
		//}

		// start bound checking when enemy is within playing field
		if(hasMoveIntoView) {
			#region still fighting
			if(!returnToBase && direction != Config.MoveDirection.STAY_STILL) {

				#region GameState.STAGE1
				if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
					// bound checking, if cross bound then change direction
					if(gameObject.tag == Config.enemy1Tag) {
						// check left bound
						if((position.x - xOffset) <= Calculate.PixelToXCoord(0)) {
							position.x = Calculate.PixelToXCoord(0) + xOffset;
							needNewDirection = true;					
						}
						// check top bound
						if((position.y + yOffset) >= Calculate.PixelToYCoord(Config.screenHeight)) {
							position.y = Calculate.PixelToYCoord(Config.screenHeight) - yOffset;
							needNewDirection = true;
						}
						// check right bound
						if((position.x + xOffset) >= Calculate.PixelToXCoord(Config.mazeLeftBound)) {
							position.x = Calculate.PixelToXCoord(Config.mazeLeftBound) - xOffset;
							needNewDirection = true;
						}
						// check bottom bound
						if((position.y - yOffset) <= Calculate.PixelToYCoord(0)){
							position.y = Calculate.PixelToYCoord(0) + yOffset;
							needNewDirection = true;
						}
					}
					else {
						// enemy2
						// check left bound
						if((position.x - xOffset) <= Calculate.PixelToXCoord(Config.mazeRightBound)) {
							position.x = Calculate.PixelToXCoord(Config.mazeRightBound) + xOffset;
							needNewDirection = true;
						}
						// check top bound
						if((position.y + yOffset) >= Calculate.PixelToYCoord(Config.screenHeight)) {
							position.y = Calculate.PixelToYCoord(Config.screenHeight) - yOffset;
							needNewDirection = true;
						}
						// check right bound
						if((position.x + xOffset) >= Calculate.PixelToXCoord(Config.screenWidth)) {
							position.x = Calculate.PixelToXCoord(Config.screenWidth) - xOffset;
							needNewDirection = true;
						}
						// check bottom bound
						if((position.y - yOffset) <= Calculate.PixelToYCoord(0)){
							position.y = Calculate.PixelToYCoord(0) + yOffset;
							needNewDirection = true;
						}
					}
				}
				#endregion

				#region GameState.STAGE2
				else {
					// check left bound
					if((position.x - xOffset) <= Calculate.PixelToXCoord(0)) {
						position.x = Calculate.PixelToXCoord(0) + xOffset;
						needNewDirection = true;
					}
					// check top bound
					if((position.y + yOffset) >= Calculate.PixelToYCoord(Config.screenHeight)) {
						position.y = Calculate.PixelToYCoord(Config.screenHeight) - yOffset;
						needNewDirection = true;
					}
					// check right bound
					if((position.x + xOffset) >= Calculate.PixelToXCoord(Config.screenWidth)) {
						position.x = Calculate.PixelToXCoord(Config.screenWidth) - xOffset;
						needNewDirection = true;
					}
					// check bottom bound
					if((position.y - yOffset) <= Calculate.PixelToYCoord(0)){
						position.y = Calculate.PixelToYCoord(0) + yOffset;
						needNewDirection = true;
					}
				}
				#endregion

				if(needNewDirection) {
					NewDirBaseOnCurrent();
					needNewDirection = false;
				}
			}
			#endregion
		}

		transform.position = position;		
	
		// check if enemy has fully enter into view 
		if(!hasMoveIntoView && (position.y < (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) 
			hasMoveIntoView = true;
	}

	void RandNewDirection(bool returnToBase) {
		int max = 10;
		if(returnToBase)	max = 9;

		switch (UnityEngine.Random.Range(1, max)) {
			case 1:
				direction = Config.MoveDirection.UP;
				break;
			case 2:
				direction = Config.MoveDirection.DOWN;
				break;
			case 3:
				direction = Config.MoveDirection.LEFT;
				break;
			case 4:
				direction = Config.MoveDirection.RIGHT;
				break;
			case 5:
				direction = Config.MoveDirection.LEFT_UP;
				break;
			case 6:
				direction = Config.MoveDirection.LEFT_DOWN;
				break;
			case 7:
				direction = Config.MoveDirection.RIGHT_DOWN;
				break;
			case 8:
				direction = Config.MoveDirection.RIGHT_UP;
				break;
			case 9:
				direction = Config.MoveDirection.STAY_STILL;
				break;
		}
	}

	void NewDirBaseOnCurrent() {
		#region GameState.STAGE1
		if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
			// base on current position and direction
			#region left, left_up, left_down
			if(direction == Config.MoveDirection.LEFT ||
			   direction == Config.MoveDirection.LEFT_DOWN ||
			   direction == Config.MoveDirection.LEFT_UP) 
			{
				if(gameObject.tag == Config.enemy1Tag) {
					// check upper left hand corner
					if( (position.x == (Calculate.PixelToXCoord(0) + xOffset)) && (position.y == (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.DOWN;
								break;
							case 2: direction = Config.MoveDirection.RIGHT_DOWN;
								break;
							case 3: direction = Config.MoveDirection.RIGHT;
								break;
						}
					}
					// check lower left hand corner
					else if( (position.x == (Calculate.PixelToXCoord(0) + xOffset)) && (position.y == (Calculate.PixelToYCoord(0) + yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.RIGHT_UP;
								break;
							case 3: direction = Config.MoveDirection.RIGHT;
								break;
						}
					}
					// left
					else {
						switch(UnityEngine.Random.Range(1, 6)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.RIGHT_UP;
								break;
							case 3: direction = Config.MoveDirection.RIGHT;
								break;
							case 4: direction = Config.MoveDirection.RIGHT_DOWN;
								break;
							case 5: direction = Config.MoveDirection.DOWN;
								break;
						}
					}
				}
				else if(gameObject.tag == Config.enemy2Tag){
					// check upper left hand corner
					if( (position.x == (Calculate.PixelToXCoord(Config.mazeRightBound) + xOffset)) && (position.y == (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.DOWN;
								break;
							case 2: direction = Config.MoveDirection.RIGHT_DOWN;
								break;
							case 3: direction = Config.MoveDirection.RIGHT;
								break;
						}
					}
					// check lower left hand corner
					else if( (position.x == (Calculate.PixelToXCoord(Config.mazeRightBound) + xOffset)) && (position.y == (Calculate.PixelToYCoord(0) + yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.RIGHT_UP;
								break;
							case 3: direction = Config.MoveDirection.RIGHT;
								break;
						}
					}
					// left
					else {
						switch(UnityEngine.Random.Range(1, 6)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.RIGHT_UP;
								break;
							case 3: direction = Config.MoveDirection.RIGHT;
								break;
							case 4: direction = Config.MoveDirection.RIGHT_DOWN;
								break;
							case 5: direction = Config.MoveDirection.DOWN;
								break;
						}
					}
				}
			}
			#endregion

			#region right, right_up, right_down
			else if(direction == Config.MoveDirection.RIGHT ||
					direction == Config.MoveDirection.RIGHT_UP ||
					direction == Config.MoveDirection.RIGHT_DOWN) 
			{
				if(gameObject.tag == Config.enemy1Tag) {
					// check upper right hand corner
					if( (position.x == (Calculate.PixelToXCoord(Config.mazeLeftBound) - xOffset)) && (position.y == (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.DOWN;
								break;
							case 2: direction = Config.MoveDirection.LEFT_DOWN;
								break;
							case 3: direction = Config.MoveDirection.LEFT;
								break;
						}
					}
					// check lower right hand corner
					else if( (position.x == (Calculate.PixelToXCoord(Config.mazeLeftBound) - xOffset)) && (position.y == (Calculate.PixelToYCoord(0) + yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.LEFT_UP;
								break;
							case 3: direction = Config.MoveDirection.LEFT;
								break;
						}
					}
					// right
					else {
						switch(UnityEngine.Random.Range(1, 6)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.LEFT_UP;
								break;
							case 3: direction = Config.MoveDirection.LEFT;
								break;
							case 4: direction = Config.MoveDirection.LEFT_DOWN;
								break;
							case 5: direction = Config.MoveDirection.DOWN;
								break;
						}
					}
				}
				else if(gameObject.tag == Config.enemy2Tag){
					// check upper right hand corner
					if( (position.x == (Calculate.PixelToXCoord(Config.screenWidth) - xOffset)) && (position.y == (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.DOWN;
								break;
							case 2: direction = Config.MoveDirection.LEFT_DOWN;
								break;
							case 3: direction = Config.MoveDirection.LEFT;
								break;
						}
					}
					// check lower left hand corner
					else if( (position.x == (Calculate.PixelToXCoord(Config.screenWidth) - xOffset)) && (position.y == (Calculate.PixelToYCoord(0) + yOffset)) ) {
						switch(UnityEngine.Random.Range(1, 4)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.LEFT_UP;
								break;
							case 3: direction = Config.MoveDirection.LEFT;
								break;
						}
					}
					// right
					else {
						switch(UnityEngine.Random.Range(1, 6)) {
							case 1: direction = Config.MoveDirection.UP;
								break;
							case 2: direction = Config.MoveDirection.LEFT_UP;
								break;
							case 3: direction = Config.MoveDirection.LEFT;
								break;
							case 4: direction = Config.MoveDirection.LEFT_DOWN;
								break;
							case 5: direction = Config.MoveDirection.DOWN;
								break;
						}
					}
				}
			}
			#endregion

			else if(direction == Config.MoveDirection.UP) {
				switch(UnityEngine.Random.Range(1, 6)) {
					case 1: direction = Config.MoveDirection.LEFT;
						break;
					case 2: direction = Config.MoveDirection.LEFT_DOWN;
						break;
					case 3: direction = Config.MoveDirection.DOWN;
						break;
					case 4: direction = Config.MoveDirection.RIGHT_DOWN;
						break;
					case 5: direction = Config.MoveDirection.RIGHT;
						break;
				}
			}
			else if(direction == Config.MoveDirection.DOWN) {
				switch(UnityEngine.Random.Range(1, 6)) {
					case 1: direction = Config.MoveDirection.LEFT;
						break;
					case 2: direction = Config.MoveDirection.LEFT_UP;
						break;
					case 3: direction = Config.MoveDirection.UP;
						break;
					case 4: direction = Config.MoveDirection.RIGHT_UP;
						break;
					case 5: direction = Config.MoveDirection.RIGHT;
						break;
				}
			}
		}
		#endregion

		#region GameState.STAGE2
		else {
			// base on current position and direction
			#region left, left_up, left_down
			if(direction == Config.MoveDirection.LEFT ||
			   direction == Config.MoveDirection.LEFT_DOWN ||
			   direction == Config.MoveDirection.LEFT_UP) 
			{						
				// check upper left hand corner
				if( (position.x == (Calculate.PixelToXCoord(0) + xOffset)) && (position.y == (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) {
					switch(UnityEngine.Random.Range(1, 4)) {
						case 1: direction = Config.MoveDirection.DOWN;
							break;
						case 2: direction = Config.MoveDirection.RIGHT_DOWN;
							break;
						case 3: direction = Config.MoveDirection.RIGHT;
							break;
					}
				}
				// check lower left hand corner
				else if( (position.x == (Calculate.PixelToXCoord(0) + xOffset)) && (position.y == (Calculate.PixelToYCoord(0) + yOffset)) ) {
					switch(UnityEngine.Random.Range(1, 4)) {
						case 1: direction = Config.MoveDirection.UP;
							break;
						case 2: direction = Config.MoveDirection.RIGHT_UP;
							break;
						case 3: direction = Config.MoveDirection.RIGHT;
							break;
					}
				}
				// left
				else {
					switch(UnityEngine.Random.Range(1, 6)) {
						case 1: direction = Config.MoveDirection.UP;
							break;
						case 2: direction = Config.MoveDirection.RIGHT_UP;
							break;
						case 3: direction = Config.MoveDirection.RIGHT;
							break;
						case 4: direction = Config.MoveDirection.RIGHT_DOWN;
							break;
						case 5: direction = Config.MoveDirection.DOWN;
							break;
					}
				}
				
			}
			#endregion

			#region right, right_up, right_down
			else if(direction == Config.MoveDirection.RIGHT ||
					direction == Config.MoveDirection.RIGHT_UP ||
					direction == Config.MoveDirection.RIGHT_DOWN) 
			{
				// check upper right hand corner
				if( (position.x == (Calculate.PixelToXCoord(Config.screenWidth) - xOffset)) && (position.y == (Calculate.PixelToYCoord(Config.screenHeight) - yOffset)) ) {
					switch(UnityEngine.Random.Range(1, 4)) {
						case 1: direction = Config.MoveDirection.DOWN;
							break;
						case 2: direction = Config.MoveDirection.LEFT_DOWN;
							break;
						case 3: direction = Config.MoveDirection.LEFT;
							break;
					}
				}
				// check lower right hand corner
				else if( (position.x == (Calculate.PixelToXCoord(Config.screenWidth) - xOffset)) && (position.y == (Calculate.PixelToYCoord(0) + yOffset)) ) {
					switch(UnityEngine.Random.Range(1, 4)) {
						case 1: direction = Config.MoveDirection.UP;
							break;
						case 2: direction = Config.MoveDirection.LEFT_UP;
							break;
						case 3: direction = Config.MoveDirection.LEFT;
							break;
					}
				}
				// right
				else {
					switch(UnityEngine.Random.Range(1, 6)) {
						case 1: direction = Config.MoveDirection.UP;
							break;
						case 2: direction = Config.MoveDirection.LEFT_UP;
							break;
						case 3: direction = Config.MoveDirection.LEFT;
							break;
						case 4: direction = Config.MoveDirection.LEFT_DOWN;
							break;
						case 5: direction = Config.MoveDirection.DOWN;
							break;
					}
				}			
			}
			#endregion

			else if(direction == Config.MoveDirection.UP) {
				switch(UnityEngine.Random.Range(1, 6)) {
					case 1: direction = Config.MoveDirection.LEFT;
						break;
					case 2: direction = Config.MoveDirection.LEFT_DOWN;
						break;
					case 3: direction = Config.MoveDirection.DOWN;
						break;
					case 4: direction = Config.MoveDirection.RIGHT_DOWN;
						break;
					case 5: direction = Config.MoveDirection.RIGHT;
						break;
				}
			}
			else if(direction == Config.MoveDirection.DOWN) {
				switch(UnityEngine.Random.Range(1, 6)) {
					case 1: direction = Config.MoveDirection.LEFT;
						break;
					case 2: direction = Config.MoveDirection.LEFT_UP;
						break;
					case 3: direction = Config.MoveDirection.UP;
						break;
					case 4: direction = Config.MoveDirection.RIGHT_UP;
						break;
					case 5: direction = Config.MoveDirection.RIGHT;
						break;
				}
			}
		}
		#endregion
	}

	void OnCollisionEnter2D(Collision2D coll) {
		// only need to worry about colliding with bullets
		if(coll.gameObject.tag == Config.p1BulletTag || coll.gameObject.tag == Config.p2BulletTag) {
			--health;
			
			if(health <= 0) {
				GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioControl>().PlaySfx(Config.SoundEffect.EXPLOSION, null);
				coll.gameObject.GetComponent<BulletState>().parent.GetComponent<PInternalState>().AddPoint(Config.enemyKillPoint);
				this.gameObject.SetActive(false);
			}
		}
	}
}
