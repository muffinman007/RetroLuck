﻿using UnityEngine;
using System.Collections;

public class AudioControl : MonoBehaviour {
	public AudioClip chomp;

	public AudioClip player1Bullet;
	public AudioClip player2Bullet;

	public AudioClip enemy1Bullet;
	public AudioClip enemy2Bullet;

	public AudioClip[] explosionArray;
	public AudioClip[] hitArray;

	public AudioClip warp;

	System.Random rand = new System.Random(System.DateTime.Now.Millisecond);


	public void PlaySfx(Config.SoundEffect type, string tag) {
		if(type == Config.SoundEffect.CHOMP) {
			GetComponent<AudioSource>().PlayOneShot(chomp);
		}
		else if(type == Config.SoundEffect.BULLET) {
			if(tag == Config.player1Tag)		GetComponent<AudioSource>().PlayOneShot(player1Bullet);
			else if(tag == Config.player2Tag)	GetComponent<AudioSource>().PlayOneShot(player2Bullet);
			else if(tag == Config.enemy1Tag)	GetComponent<AudioSource>().PlayOneShot(enemy1Bullet);
			else if(tag == Config.enemy2Tag)	GetComponent<AudioSource>().PlayOneShot(enemy2Bullet);
		}
		else if(type == Config.SoundEffect.EXPLOSION) {
			GetComponent<AudioSource>().PlayOneShot(explosionArray[rand.Next(0, explosionArray.Length)]);
		}
		else if(type == Config.SoundEffect.HIT) {
			GetComponent<AudioSource>().PlayOneShot(hitArray[rand.Next(0, hitArray.Length)]);
		}
		else if(type == Config.SoundEffect.WARP) {
			GetComponent<AudioSource>().PlayOneShot(warp);
		}
	}

}
