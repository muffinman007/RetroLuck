﻿using UnityEngine;
using System.Collections;

public class InterOpData : MonoBehaviour {
	public Config.GameState state;
	public int player1Score;
	public int player2Score;
	public string whoGoesFirst;

	public static InterOpData self;
	

	void Awake() {		
		if(self) 
			DestroyImmediate(gameObject);
		else { 
			DontDestroyOnLoad(this);
			state = Config.GameState.INTRO;
			self = this;
		}
	}	

}
