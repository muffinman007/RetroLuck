﻿using UnityEngine;
using System.Collections;

public class P1Internal : PInternalState {	

	protected override void Start() {
		if(InterOpData.GetComponent<InterOpData>().state == Config.GameState.STAGE1) {
			xCoord = Config.mazeLeftBound - Config.margin;
			yCoord = Config.screenHeight;
		}
		else {
			xCoord = Config.screenWidth;
			yCoord = 480;
		}
		base.Start();
	}

	// Update is called once per frame
	protected override void Update () {
		Vector3 move = transform.position;

		// player1 movement
		if(Input.GetKey(KeyCode.A) || (Input.GetAxis("HorizontalP1") == -1)) move.x -= (Time.deltaTime * speed);				// left
		if(Input.GetKey(KeyCode.D) || (Input.GetAxis("HorizontalP1") ==  1)) move.x += (Time.deltaTime * speed);				// right
		if(Input.GetKey(KeyCode.W) || (Input.GetAxis("VerticalP1")   ==  1)) move.y += (Time.deltaTime * speed);				// up
		if(Input.GetKey(KeyCode.S) || (Input.GetAxis("VerticalP1")   == -1)) move.y -= (Time.deltaTime * speed);				// down

		// boundary check
		Vector3 p1Size = this.GetComponent<SpriteRenderer>().bounds.extents;

		if(move.x <= (Calculate.PixelToXCoord(0) + p1Size.x))
			move.x = Calculate.PixelToXCoord(0) + p1Size.x;
		if(move.x >= (Calculate.PixelToXCoord(xCoord) - p1Size.x))
			move.x = Calculate.PixelToXCoord(xCoord) - p1Size.x;
		if(move.y <= (Calculate.PixelToYCoord(0) + p1Size.y))
			move.y = Calculate.PixelToYCoord(0) + p1Size.y;
		if(move.y >= (Calculate.PixelToYCoord(yCoord) - p1Size.y))
			move.y = Calculate.PixelToYCoord(yCoord) - p1Size.y;

		// update player movement
		this.transform.position = move;

		// check fire trigger
		if(HasDot) {
			if(Input.GetKeyUp(Config.p1FireBtn) || Input.GetKeyUp(KeyCode.Joystick1Button0)) FireDotCreature();
			timer = 0.0f;
		}
		else {
			base.Update();
		}
	}




}
