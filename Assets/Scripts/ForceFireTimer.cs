﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ForceFireTimer : MonoBehaviour {
	bool isP1Dirty;
	bool isP2Dirty;

	public GameObject player1Timer;
	public GameObject player2Timer;

	GameObject player1;
	GameObject player2;

	// Use this for initialization
	void Start () {
		player1 = GameObject.FindGameObjectWithTag(Config.player1Tag);
		player2 = GameObject.FindGameObjectWithTag(Config.player2Tag);	
	}
	
	// Update is called once per frame
	void Update () {
		if(player1 == null)
			player1 = GameObject.FindGameObjectWithTag(Config.player1Tag);
		else if(player2 == null)
			player2 = GameObject.FindGameObjectWithTag(Config.player2Tag);

		// one or the other has the dot OR none has the dot
		if(player1.GetComponent<PInternalState>().HasDot) {
			player1Timer.GetComponent<Slider>().value -= Time.deltaTime;
			player1Timer.transform.FindChild("Background").GetComponent<Image>().color = 
				new Color(player1Timer.transform.FindChild("Background").GetComponent<Image>().color.r, 
						  player1Timer.transform.FindChild("Background").GetComponent<Image>().color.g - Time.deltaTime,
						  player1Timer.transform.FindChild("Background").GetComponent<Image>().color.b - Time.deltaTime);

			if(player1Timer.GetComponent<Slider>().value <= 0.0f)	player1.GetComponent<PInternalState>().FireDotCreature();
			isP1Dirty = true;
		}
		else if(player2.GetComponent<PInternalState>().HasDot) {
			player2Timer.GetComponent<Slider>().value -= Time.deltaTime;
			player2Timer.transform.FindChild("Background").GetComponent<Image>().color = 
				new Color(player2Timer.transform.FindChild("Background").GetComponent<Image>().color.r, 
						  player2Timer.transform.FindChild("Background").GetComponent<Image>().color.g - Time.deltaTime,
						  player2Timer.transform.FindChild("Background").GetComponent<Image>().color.b - Time.deltaTime);

			if(player2Timer.GetComponent<Slider>().value <= 0.0f)	player2.GetComponent<PInternalState>().FireDotCreature();

			isP2Dirty = true;
		}
		else {
			if(isP1Dirty) {
				player1Timer.GetComponent<Slider>().value = player1Timer.GetComponent<Slider>().maxValue;
				player1Timer.transform.FindChild("Background").GetComponent<Image>().color = Color.white;
				isP1Dirty = false;
			}
			if(isP2Dirty) {
				player2Timer.GetComponent<Slider>().value = player2Timer.GetComponent<Slider>().maxValue;
				player2Timer.transform.FindChild("Background").GetComponent<Image>().color = Color.white;
				isP2Dirty = false;
			}
		}
	}

	
}
