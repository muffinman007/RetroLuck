﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class MapControl : MonoBehaviour {

	// 36 x 35 array
	List<GameObject> foodObjList = new List<GameObject>();
	System.Random rand = new System.Random();

	// Use this for initialization
	void Start(){
	    LoadMap();
	}

    // helper methods
    void LoadMap(){
		// to hold map data in order to find the correction location to place the food dots and special dots
		int row			= Config.screenHeight / Config.blockDimension; 
		int column		= (Config.mazeRightBound - Config.mazeLeftBound) / Config.blockDimension;
		byte[,] mapData = new byte[row, column];
		// to help placed the data in the correct index of the mapData array
		int i			= 0;								
		int j			= 0;
		GameObject foodObj;
		Vector2 coordinate = new Vector2();
		Vector2[,] locationDataArray = new Vector2[row, column];

		// first select a color for the block tint		
		Color color;
		int r = 0, g = 0, b = 0;
		do {
			r = rand.Next(90, 256);
			g = rand.Next(90, 256);
			b = rand.Next(90, 256);
		}while((r == g) || (r == b) || (g == b));
		color = new Color(r / 255.0f, g / 255.0f, b / 255.0f);


		// load data from textfile and create the map
		TextAsset assetData = Resources.Load<TextAsset>(Config.mapPath);
		string[] textData = assetData.text.Split(new string[] {System.Environment.NewLine}, System.StringSplitOptions.None);
		
		int yOffset = Config.screenHeight;
		int xOffset = 0;
		GameObject block;
		GameObject guide = this.gameObject;								// need to assign something so error won't appear
		bool instantiateGuide = false;

		foreach(string txt in textData) {							
			string[] data = txt.Split(new char[]{ ',' }, System.StringSplitOptions.RemoveEmptyEntries);
			foreach(string code in data) {

				coordinate = new Vector2(Calculate.PixelToXCoord(Config.mazeLeftBound + xOffset),
										 Calculate.PixelToYCoord(yOffset));

				#region Wall
				if(code.Equals(Config.code0)) {					
					if(xOffset == 0) {
						block = Resources.Load<GameObject>("Prefabs/destroyBlockLeft");
					}
					else if(xOffset == ((Config.mazeRightBound - Config.mazeLeftBound) - Config.blockDimension)) {
						block = Resources.Load<GameObject>("Prefabs/destroyBlockRight");
					}
					else {
						block = Resources.Load<GameObject>("Prefabs/MazeBlock");
					}
					// create block and set properties					
					block.transform.position = new Vector3(	coordinate.x,
															coordinate.y,
															0.0f);
					block.GetComponent<SpriteRenderer>().color = color;
					Instantiate<GameObject>(block);
				}
				#endregion

				#region mazeOpening guide
				else if(code.Equals(Config.codeOpenAllFour)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenAllFour);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenBottom)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenBottom);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenBtmRght)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenBtmRght);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenBtmRghtTop)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenBtmRghtTop);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenLeft)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenLeft);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenLftBtm)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenLftBtm);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenLftBtmRght)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenLftBtmRght);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenLftRght)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenLftRght);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenRghTopLft)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenRghTopLft);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenRight)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenRight);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenTop)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenTop);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenTopBtm)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenTopBtm);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenTopLft)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenTopLft);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenTopLftBtm)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenTopLftBtm);
					instantiateGuide = true;
				}
				else if(code.Equals(Config.codeOpenTopRght)) {
					guide = Resources.Load<GameObject>("Prefabs/" + Config.OpenTopRght);
					instantiateGuide = true;
				}				

				if(instantiateGuide) {
					guide.transform.position = new Vector3(Calculate.PixelToXCoord(Config.mazeLeftBound + xOffset + Config.blockDimension),
														   Calculate.PixelToYCoord(yOffset - Config.blockDimension),
														   0.0f);
					Instantiate<GameObject>(guide);
					instantiateGuide = false;
				}
				#endregion

				#region adding data to mapData and locationDataArray
				if(code.Equals(Config.code0)) {
					mapData[i,j] = 0;
				}
				else {
					mapData[i, j] = 1;
				}

				locationDataArray[i, j] = coordinate;
				++j;
				#endregion

				xOffset += Config.blockDimension;
			}
				
			xOffset = 0;
			yOffset -= Config.blockDimension;	
			++i;
			j = 0;	
		}
		
		rand = new System.Random(System.DateTime.Now.Millisecond);
		// new color
		xOffset		= r;							// temporary holder
		yOffset		= b;							// temporary holder
		i			= g;							// temporary holder
		do {
			r = rand.Next(20, 256);
			g = rand.Next(20, 256);
			b = rand.Next(20, 256);
		}while((r == xOffset) || (b == yOffset) || (g == i) || (r == g) || (r == b) || (g == b));

		// creat the foods
		// i starts at 1 because row 0 will have all code 0, the maze has all walls at the top and bottom of the screen
		for(i = 1; i < row; ++i) {
			for(j = 1; j < column; ++j) {
				if(mapData[i, j] == 1 && mapData[i, j - 1] == 1 && mapData[i - 1, j] == 1 && mapData[i - 1, j - 1] == 1) {
					foodObj = Resources.Load<GameObject>("Prefabs/" + Config.foodTag);
					foodObj.GetComponent<SpriteRenderer>().color = new Color(r / 255.0f, g / 255.0f, b / 255.0f); 
					foodObj.transform.position = new Vector3(locationDataArray[i, j].x, locationDataArray[i, j].y, 0.0f);
					Instantiate<GameObject>(foodObj);

					foodObjList.Add(foodObj);
				}					
			}
		}
							
    }
	

	public void AddSpecial() {
		
	}


}
