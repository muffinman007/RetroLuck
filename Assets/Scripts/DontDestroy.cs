﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {
	public static DontDestroy self;
	void Awake() {

		if(self) 
			DestroyImmediate(gameObject);
		else { 
			DontDestroyOnLoad(this);
			self = this;
		}
	}
}
