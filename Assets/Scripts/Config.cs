﻿using UnityEngine;
using System.Collections;

public static class Config{

    public const int screenWidth			= 1200;
    public const int screenHeight			= 700;
    public const float onePixelInUnitTerm	= 0.01f;            // math: default sprite pixels to unit is 100. which is 100px = 1unit . so 1px = 0.01unit

	// maze boundary
	public const int mazeLeftBound			= 240;				// base on resolution of 1200 x 700
	public const int mazeRightBound			= 960;				// 

	// maze block size
	public const int blockDimension			= 20;				// using a 20px x 20px block

	public const int margin					= 40;	

	public const string player1Tag			= "Player1";
	public const string player2Tag			= "Player2";
	public const string p1BulletTag			= "P1Bullet";
	public const string p2BulletTag			= "P2Bullet";	
	public const string dotCreatureTag		= "DotCreature";
	public const string foodTag				= "Food";
	public const string destroyBlockLeft	= "DestroyBlockLeft";
	public const string destroyBlockRight	= "DestroyBlockRight";

	public const float	pBulletSpeed		= 7.0f;
	public const int	enemyMinBulletSpd	= 5;
	public const int	enemyMaxBulletSpd	= 7;

	// movement direction of the dot creature
	public enum MoveDirection { UP, DOWN, LEFT, RIGHT, LEFT_UP, LEFT_DOWN, RIGHT_DOWN, RIGHT_UP, STAY_STILL}

	// map filename
	public const string mapPath			= "Maps/map1v3";

	// maze opening collider tag, to help guide the dot traversal through the maze
	public const string OpenTop				= "OpenTop";
	public const string OpenLeft			= "OpenLeft";
	public const string OpenBottom			= "OpenBottom";
	public const string OpenRight			= "OpenRight";
	public const string OpenTopLft			= "OpenTopLft";
	public const string OpenTopBtm			= "OpenTopBtm";
	public const string OpenTopRght			= "OpenTopRght";
	public const string OpenLftRght			= "OpenLftRght";
	public const string OpenLftBtm			= "OpenLftBtm";
	public const string OpenBtmRght			= "OpenBtmRght";
	public const string OpenTopLftBtm		= "OpenTopLftBtm";
	public const string OpenLftBtmRght		= "OpenLftBtmRght";
	public const string OpenBtmRghtTop		= "OpenBtmRghtTop";
	public const string OpenRghTopLft		= "OpenRghTopLft";
	public const string OpenAllFour			= "OpenAllFour";

	// map code. the code used when desiging the map
	public const string code0				= "0";
	public const string codeOpenTop			= "a";
	public const string codeOpenLeft		= "b";
	public const string codeOpenBottom		= "c";
	public const string codeOpenRight		= "d";
	public const string codeOpenTopLft		= "e";
	public const string codeOpenTopBtm		= "f";
	public const string codeOpenTopRght		= "g";
	public const string codeOpenLftRght		= "i";
	public const string codeOpenLftBtm		= "h";
	public const string codeOpenBtmRght		= "j";
	public const string codeOpenTopLftBtm	= "k";
	public const string codeOpenLftBtmRght	= "l";
	public const string codeOpenBtmRghtTop	= "m";
	public const string codeOpenRghTopLft	= "n";
	public const string codeOpenAllFour		= "o";

	// layermask to use in raycast collision detection for the dot creature
	public const string layerMaskName		= "OpenGuide";

	// points
	public const int minusPoint				= 100;
	public const int bonusPoint				= 400;
	public const int enemyKillPoint			= 200;
	public const int selfKillMinusPoint		= 50;
	public const int hitMinusPoint			= 10;
	public const int foodPoint				= 10;

	// gui 
	public const string scoreCanvas			= "ScoreCanvas";
	public const string player1ScoreTxt		= "Player1ScoreTxt";
	public const string player2ScoreTxt		= "Player2ScoreTxt";
	public const string Player1TextName		= "Player1Text";
	public const string Player2TextName		= "Player2Text";

	// firing button
	public const KeyCode p1FireBtn			= KeyCode.LeftShift;
	public const KeyCode p2FireBtn			= KeyCode.KeypadEnter;


	// sound effects
	public enum SoundEffect { BULLET, CHOMP, HIT, EXPLOSION, WARP }

	// enemies
	public const string enemy1Tag			= "Enemy1";
	public const string enemy2Tag			= "Enemy2";
	public const string enemyBulletTag		= "EnemyBullet";

	// interop data
	public enum GameState { INTRO, STAGE1, STAGE2, OVER }

	// interop data
	public const string InterOpTag			= "InterOpData";

	// Time master timer, in sec
	public const float getReadyTime			= 3.0f;
	public const float timeLeftTime			= 10.0f;
	public const float maxRoundTime			= 30.0f;
}
