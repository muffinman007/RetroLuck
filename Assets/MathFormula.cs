﻿/*

For notes:

formula to pixel y coordinate to unity coordinate base on default "pixels per unit" value of 100.

y = (1/pixels per unit) * (desired y coordinate in pixel - (resolution height / 2))
x = (1/pixels per unit) * (desired x coordinate in pixel - (resolution width / 2))



*/